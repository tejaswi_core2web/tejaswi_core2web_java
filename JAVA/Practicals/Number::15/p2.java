import java.util.*;

class PrimeNumber {

	public static void main(String[] args) {

		System.out.println("Enter Number :");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();

		int cnt =0;
		for(int i =1;i<num; i++) {
			if(num%i==0) {
				cnt++;
			}
		}

		if(cnt==1){
			System.out.println(num + "is prime number");
		}
		else {
			System.out.println(num +" is not a prime number");
		}
	}
}
