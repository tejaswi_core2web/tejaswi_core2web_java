import java.util.*;

class ReverseNum {

	public static void main(String[] args) {

		System.out.println("Enter Number:");
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();


		int rev=0;
		int rem=0;

		while(num>0){
			rem = num %10;
			rev = rem + rev*10;
			num= num/10;

		}
		System.out.println("Reverse of" + num + " is " + rev);
	}
}

