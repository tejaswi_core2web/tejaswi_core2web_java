

class Outer9{

	static int x = 10;
	int y = 20;

	static void run() {
		System.out.println("In static");
	}

	void fun() {
			System.out.println("In non static");
		}

	static class Inner{
		Inner() {
			System.out.println(x);
			run();
		}
	}
}
class Client{

	public static void main(String[] args){

		Outer9.Inner obj = new Outer9.Inner();
	}
}
