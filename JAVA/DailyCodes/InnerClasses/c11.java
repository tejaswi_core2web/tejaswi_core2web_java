

class Demo{
	void fun() {
		System.out.println("In fun");
	}
}
class DemoChild extends Demo {
	void fun() {
		System.out.println("In fun DemoChild");
	}
	void run(){
		System.out.println("In run demoChild");
	}
}
class Outer{

	public static void main(String[] args) {

		Demo obj = new DemoChild();
		obj.fun();
	}
}
