

class Outer3 {

	Outer3() {
		System.out.println("Outer Constructor");
	}

	class Inner{

		Inner(){
			System.out.println("Inner Constructor");
		}
	}

	public static void main(String[] args) {

		Outer3 outObj1 = new Outer3();
		Outer3 outObj2 = new Outer3();

		Inner obj=outObj2.new Inner();
	}
}
