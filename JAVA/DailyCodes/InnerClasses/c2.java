
class Outer1 {

	class Inner{
		Inner(){
			System.out.println("In inner constructor");
		}
	}

	public static void main(String[] args) {
		
		Outer1 outObj = new Outer1();
		Inner obj = outObj.new Inner();
	}

}
