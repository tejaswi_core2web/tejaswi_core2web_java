


class Outer5 {

	int x=10;
	static int y=20;

		Outer5(){
			System.out.println("Outer Constructor");
		}

		class Inner {

			int x=30;
			Inner(int x) {

				System.out.println("Inner Constructor");
				System.out.println(this.x);
				System.out.println(y);
				System.out.println(x);
				System.out.println(Outer5.this.x);

			}
		}

		public static void main(String[] args) {

			Outer5 obj1 = new Outer5();
			Inner obj = obj1.new Inner(120);
		}
}
