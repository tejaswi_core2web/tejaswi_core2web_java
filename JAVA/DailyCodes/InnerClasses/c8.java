


class Outer8 {

	static int x=10;
	int y=20;

	void run(){
		System.out.println("In static");
	}

	void fun(){
		System.out.println("In Non-static");
	}

	public static void main(String[] args) {

		Outer8 obj = new Outer8();

		System.out.println(obj.x);
		System.out.println(obj.y);
		obj.run();
		obj.fun();
	}
}
