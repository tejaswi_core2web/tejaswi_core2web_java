class Demo{

	void fun(){
		System.out.println("In fun");
	}
}

class Outer {
	public static void main(String[] args) {

		Demo obj = new Demo(){

			void fun() {
				System.out.println("In fun Outer$1");
			}
		};
		obj.fun();
	}
}
