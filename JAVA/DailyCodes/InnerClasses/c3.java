

class Outer2 {

	Outer2() {
		System.out.println("In outre constructor");
	}

	class Inner{

		Inner() {
			System.out.println("In inner constructor");
		}
	}
	public static void main(String[] args) {
		Outer2 obj = new Outer2();
		Inner obj1 = obj.new Inner();
	}
}
