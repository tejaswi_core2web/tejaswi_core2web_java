interface Parent {

	void fun();
	void run();

}

class Child implements Parent {

	public void fun() {
		System.out.println("Alia Bhat");
	}
	public void run() {
		System.out.println("Deepika");
	}
}

class Client {
	public static void main(String[] args) {

		Parent obj = new Child();
		obj.fun();
		obj.run();
	}
}

