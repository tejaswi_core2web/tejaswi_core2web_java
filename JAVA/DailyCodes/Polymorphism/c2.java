class Demo {

	void fun(int x, float y) {
		System.out.println("int-float");
	}
	void fun(int x, int y) {
		System.out.println("int-int");
	}
	void fun(float x, float y) {
		System.out.println("float-float");
	}
	void fun(float x, int y) {
		System.out.println("float-int");
	}

	public static void main(String[] args) {

		Demo obj = new Demo();
		obj.fun(10,20);
		obj.fun(10,20.5f);
		obj.fun(20.5f,10);
		obj.fun(20.f,20.5f);
		obj.fun('A', 10);
		obj.fun(10.5,10.5);

	}
}
