class TriPattern5 {

	public static void main(String[] args) {

		int row=4;
			
			for(int i=1;i<=row;i++){
				char ch='D';
				char ch1= 'd';

				for(int j=1;j<=i;j++){

					if(i%2==1){
						System.out.print(ch-- +" ");
					}
					else {
						System.out.print(ch1-- +" " );
					}
				}
				System.out.println();
			}
		}
}
