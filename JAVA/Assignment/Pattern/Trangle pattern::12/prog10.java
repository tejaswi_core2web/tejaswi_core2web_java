import java.util.*;

class TriPattern10 {

	public static void main(String[] args) {

		System.out.print("Enter no. of rows :");
		Scanner sc = new Scanner(System.in);
		int row= sc.nextInt();
		int num=1;
		char x='a';

		for(int i=1;i<=row; i++){

			for(int j=1;j<=i;j++){

				if(j%2==1){
					System.out.print(i + " ");
				}
				else {
					System.out.print(x + " ");
					x++;

				}
			}
			System.out.println();
		}
	}
}
		
