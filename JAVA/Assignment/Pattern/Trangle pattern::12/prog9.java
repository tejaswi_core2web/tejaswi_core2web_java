import java.util.*;

class TriPattern9 {
		public static void main(String[] args) {

			System.out.println("Enter no. of rows:");
			Scanner sc = new Scanner(System.in) ;
			int row=sc.nextInt();
			int num=1;
			char x ='a';

			for(int i=1; i<=row; i++){
				for(j=1; j<=i;j++) {

					if(j%2==1){
						System.out.print(num + " ");
					}
					else{
						System.out.print(x + " ");
					}
				
				num++;
				x++;
			}
			System.out.println();
		}
	}
}
