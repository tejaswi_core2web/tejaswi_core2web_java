import java.util.*;

class Palindrome {
	public static void main(String[] args) {
	
	System.out.println("Enter Size :") ;
	
	Scanner sc = new Scanner(System.in);
	
	int size = sc.nextInt();
	
	int arr[] = new int[size];
	
	System.out.println("Enter Array Element :");
	
	for(int i =0;i<size; i++){
	
		arr[i] = sc.nextInt();
	}
	
	int cnt=0;
	for(int i =0;i<size; i++){
		int temp = arr[i];
		int rev =0;
		int rem;
		
		while(temp != 0) {
			
			rem = temp%10;
			rev = rem+rev*10;
			temp = temp %10;
		}
		
			System.out.println(rev);
	 if(arr[i] == rev) {
	 	cnt++; 
	 
	 System.out.println(arr[i] + " is a palindrome");
	 
	 }
	 }
	 
	 System.out.println("No. of palindrome :" + cnt);
	 }
}
