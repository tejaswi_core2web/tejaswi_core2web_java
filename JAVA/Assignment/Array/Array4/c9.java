import java.util.*;

class Range{
	
	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter array Size:");
		
		int size=sc.nextInt();
		char arr[] = new char[size];
		
		System.out.println("Enter Array Element");
		
		for(int i=0;i<size;i++){
			arr[i]= sc.next().charAt(0);
		}
		
		for(int i=0;i<size;i++){
			if(arr[i]<'a' || arr[i]>'z'){
			
				arr[i]='#';
			}
		}
		
		for(int i=0;i<size;i++){
			System.out.println(arr[i] + " ");
		}
		}
	}
