import java.util.*;


class SumOddEven {
	public static void main(String[] args) {
	
	int arr[] =new int[] {10,15,9,1,12,15};
	
	Scanner sc = new Scanner(System.in);
	
	int evensum=0; 
	int oddsum=0;
	
	for(int i =0;i<arr.length; i++){
		if(arr[i]%2==0){
		
			evensum +=arr[i];
		}
		else {
			oddsum +=arr[i];
		}
	}
	
	System.out.println("Even sum :" + evensum);
	System.out.println("Odd sum :" + oddsum);
	}
}
